## [2.3.2](https://gitlab.com/to-be-continuous/gcloud/compare/2.3.1...2.3.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([4289ba1](https://gitlab.com/to-be-continuous/gcloud/commit/4289ba126db6ca607adf5304caee34f41840f09e))

## [2.3.1](https://gitlab.com/to-be-continuous/gcloud/compare/2.3.0...2.3.1) (2022-12-17)


### Bug Fixes

* hanging awk script ([73cb04d](https://gitlab.com/to-be-continuous/gcloud/commit/73cb04d2af7b3e6b17f9381eba4e3a1bc7e3e8f3))

# [2.3.0](https://gitlab.com/to-be-continuous/gcloud/compare/2.2.0...2.3.0) (2022-12-15)


### Features

* improve environments url ([f16e4bf](https://gitlab.com/to-be-continuous/gcloud/commit/f16e4bf3564bf345a7582939578d5a14e331f6e9))

# [2.2.0](https://gitlab.com/to-be-continuous/gcloud/compare/2.1.1...2.2.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([8879907](https://gitlab.com/to-be-continuous/gcloud/commit/887990766ca33b807fd507494750b1508ebe75e1))

## [2.1.1](https://gitlab.com/to-be-continuous/gcloud/compare/2.1.0...2.1.1) (2022-10-12)


### Bug Fixes

* syntax error in GCP authentication function ([2a6d02b](https://gitlab.com/to-be-continuous/gcloud/commit/2a6d02bef21016ca43f88fe09db1ab332c034785))

# [2.1.0](https://gitlab.com/to-be-continuous/gcloud/compare/2.0.0...2.1.0) (2022-10-11)


### Features

* authenticate with OpenID Connect ([b5342c4](https://gitlab.com/to-be-continuous/gcloud/commit/b5342c447cb34a61af613fed724d24e4043a6eaa))

# [2.0.0](https://gitlab.com/to-be-continuous/gcloud/compare/1.8.0...2.0.0) (2022-08-05)


### Features

* make MR pipeline the default workflow ([cb6b185](https://gitlab.com/to-be-continuous/gcloud/commit/cb6b185639eeb88acaaebceac05332f89ae0d637))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.8.0](https://gitlab.com/to-be-continuous/gcloud/compare/1.7.0...1.8.0) (2022-06-30)


### Features

* enforce AUTODEPLOY_TO_PROD and PUBLISH_ON_PROD as boolean variables ([764e662](https://gitlab.com/to-be-continuous/gcloud/commit/764e66287d903d392e56cd72dceeb9d5f7c951ae))

# [1.7.0](https://gitlab.com/to-be-continuous/gcloud/compare/1.6.2...1.7.0) (2022-05-01)


### Features

* configurable tracking image ([8a34c53](https://gitlab.com/to-be-continuous/gcloud/commit/8a34c537990e9fcbf9a0a9a7cac602d54bc6bfb8))

## [1.6.2](https://gitlab.com/to-be-continuous/gcloud/compare/1.6.1...1.6.2) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([6ec5784](https://gitlab.com/to-be-continuous/gcloud/commit/6ec5784257aa66ccf2b6fce88afd92c1a424942c))

## [1.6.1](https://gitlab.com/to-be-continuous/gcloud/compare/1.6.0...1.6.1) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([8dddea0](https://gitlab.com/to-be-continuous/gcloud/commit/8dddea0e00689cf27b928a931305bb430a9c6db8))

# [1.6.0](https://gitlab.com/to-be-continuous/gcloud/compare/1.5.2...1.6.0) (2022-01-10)


### Features

* Vault variant + non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([7e6123b](https://gitlab.com/to-be-continuous/gcloud/commit/7e6123bc581335a15230d6f7d781d017675d3737))

## [1.5.2](https://gitlab.com/to-be-continuous/gcloud/compare/1.5.1...1.5.2) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([05c2788](https://gitlab.com/to-be-continuous/gcloud/commit/05c27885d0d396c5fe4089685aae52928ff37b5b))

## [1.5.1](https://gitlab.com/to-be-continuous/gcloud/compare/1.5.0...1.5.1) (2021-10-11)


### Bug Fixes

* convert environment gcp_key_file content to file ([0a2e8eb](https://gitlab.com/to-be-continuous/gcloud/commit/0a2e8eb511913facd5b7fc3f509a8235a77570f1))

# [1.5.0](https://gitlab.com/to-be-continuous/gcloud/compare/1.4.1...1.5.0) (2021-10-10)


### Features

* manage static or dynamic environment urls ([1130b17](https://gitlab.com/to-be-continuous/gcloud/commit/1130b17fb0acdbd36e0988be85aa958433ff81d8))

## [1.4.1](https://gitlab.com/to-be-continuous/gcloud/compare/1.4.0...1.4.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([0f43e16](https://gitlab.com/to-be-continuous/gcloud/commit/0f43e16d0acf7219065c3dab392c9ddfbfe211e3))

## [1.4.0](https://gitlab.com/to-be-continuous/gcloud/compare/1.3.0...1.4.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([469f03f](https://gitlab.com/to-be-continuous/gcloud/commit/469f03f5596b0072a322e04bd5d9fa412ce57d75))

## [1.3.0](https://gitlab.com/to-be-continuous/gcloud/compare/1.2.0...1.3.0) (2021-06-19)

### Features

* support multi-lines environment variables substitution ([88dd89a](https://gitlab.com/to-be-continuous/gcloud/commit/88dd89ad5514eb56032093edc5cb81bdfb51df80))

## [1.2.0](https://gitlab.com/to-be-continuous/gcloud/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([6ae0887](https://gitlab.com/to-be-continuous/gcloud/commit/6ae088718ba0a22887b3ebe19563c1f9d243823e))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/gcloud/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([5a59857](https://gitlab.com/Orange-OpenSource/tbc/gcloud/commit/5a598570fd58469b2d8b4020cf1e832aabe602e6))

## 1.0.0 (2021-05-06)

### Features

* initial release ([9338f48](https://gitlab.com/Orange-OpenSource/tbc/gcloud/commit/9338f487bb12d1fc6527d9a03cff306906945d42))
